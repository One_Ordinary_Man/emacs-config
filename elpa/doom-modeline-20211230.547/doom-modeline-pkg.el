(define-package "doom-modeline" "20211230.547" "A minimal and modern mode-line"
  '((emacs "25.1")
    (all-the-icons "2.2.0")
    (shrink-path "0.2.0")
    (dash "2.11.0"))
  :commit "84573ae5e7db2705da67c2eda94b786a99346272" :authors
  '(("Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainer
  '("Vincent Zhang" . "seagle0128@gmail.com")
  :keywords
  '("faces" "mode-line")
  :url "https://github.com/seagle0128/doom-modeline")
;; Local Variables:
;; no-byte-compile: t
;; End:
