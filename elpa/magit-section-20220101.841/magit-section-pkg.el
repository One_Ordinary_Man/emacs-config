(define-package "magit-section" "20220101.841" "Sections for read-only buffers"
  '((emacs "25.1")
    (dash "20210826"))
  :commit "7d0d7068deff003a02f49fa430153e4611c80175" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("tools")
  :url "https://github.com/magit/magit")
;; Local Variables:
;; no-byte-compile: t
;; End:
