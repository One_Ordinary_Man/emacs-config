(define-package "with-editor" "20220101.1017" "Use the Emacsclient as $EDITOR"
  '((emacs "24.4"))
  :commit "a71eb40bb6c37ddf0e449e383c252b661d8e8103" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("tools")
  :url "https://github.com/magit/with-editor")
;; Local Variables:
;; no-byte-compile: t
;; End:
